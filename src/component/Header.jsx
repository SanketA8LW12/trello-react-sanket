import "./Header.css"
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';


function Header() {

  return (
    <AppBar
      position="static"
      sx={{
        backgroundImage: "url('https://images.pexels.com/photos/301378/pexels-photo-301378.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1')",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
      }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters sx={{justifyContent: 'center'}}>
          <Typography variant="h4">
            Sanket Task Manager
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default Header;

